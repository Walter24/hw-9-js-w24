const today = new Date(); // отримуємо сьогоднішню дату
let birthday = prompt("Enter your birthday in format: dd.mm.yyyy like", "30.12.1979"); // отримуємо дату народження як рядок

while (!isNaN(birthday) || birthday === null || birthday === " ") {   // Перевірка на введення дати у виді рядка, чи натиснули Cancel чи не ввели пробіл
	birthday = Number (prompt("Put in your birthday by shown format only")); // Запитати ввести дату тільки числом
}

let temp = birthday.split('.'); // конвертуємо рядок в масив за роздільником '.'
birthday = `${temp[2]}-${temp[1]}-${temp[0]}`; // переставляємо елементи масиву для потрібного (yyyy-mm-dd) обрахування формату щоб потім перерахувати в мілісекунди 
console.log( birthday ); // перевірка на отримання потрібного (yyyy-mm-dd) формату
// можна потрібний формат отримати як:
// Number(birthday.substring(6,10) - для року
// Number(birthday.substring(3,5))-1 - для місяця
// Number(birthday.substring(0,2)),) - для дня
const birthdayDate = new Date(birthday); // занесені дані в потрібному для обрахуання форматі переприсвоюємо щоб отримати дату народження

let difference = today - birthdayDate; // отримуємо кількість мілісекунд від дня народження до сьогоднішньої дати

let age = Math.floor(difference/1000/60/60/24/360) // формула перерахування мілісекунд в роки
alert( "You are" + " " + age + " " + "years old !" );

let day = birthdayDate.getDate(); // отримуємо дату народження (числом)
let month = birthdayDate.getMonth()+1; // отримуємо місяць народження (числом) поправка на отримання місяця (бо місяці починаються з 0)
let year = birthdayDate.getFullYear(); // отримуємо рік народження (числом)

const zodiacSigns = {
	
    "aquarius": "Водолій - The Water Bearer",
    "pisces": "Риби - The Fish",
    "aries": "Овен - The Ram",
    "taurus": "Тiлець - The Bull",
    "gemini": "Близнюки - The Twins",
    "cancer": "Рак - The Crab",
    "leo": "Лев - The Lion",
    "virgo": "Діва - The Virgin",
    "libra": "Ваги - The Scales",
    "scorpio": "Скорпіон - The Scorpion",
	"sagittarius": "Стрілець - The Archer",
	"capricorn": "Козеріг - The unicorn goat with tail fish"
}

if ((month === 1 && day >= 21) || (month === 2 && day <= 18)){
	alert(`Your zodiac sign is : ${zodiacSigns.aquarius}`);
}
else if ((month === 2 && day >= 19) || (month === 3 && day <= 20)){
	alert(`Your zodiac sign is : ${zodiacSigns.pisces}`);
}
else if ((month === 3 && day >= 21) || (month === 4 && day <= 20)){
	alert(`Your zodiac sign is : ${zodiacSigns.aries}`);
}	
else if ((month === 4 && day >= 21) || (month === 5 && day <= 20)){
	alert(`Your zodiac sign is : ${zodiacSigns.taurus}`);
}
else if ((month === 5 && day >= 21) || (month === 6 && day <= 20)){
	alert(`Your zodiac sign is : ${zodiacSigns.gemini}`);
}
else if ((month === 6 && day >= 22) || (month === 7 && day <= 22)){
	alert(`Your zodiac sign is : ${zodiacSigns.cancer}`);
}
else if ((month === 7 && day >= 23) || (month === 8 && day <= 23)){
	alert(`Your zodiac sign is : ${zodiacSigns.leo}`);
}
else if ((month === 8 && day >= 24) || (month === 9 && day <= 23)){
	alert(`Your zodiac sign is : ${zodiacSigns.virgo}`)
}
else if ((month === 9 && day >= 24) || (month === 10 && day <= 23)){
	alert(`Your zodiac sign is : ${zodiacSigns.libra}`);
}
else if ((month === 10 && day >= 24) || (month === 11 && day <= 22)){
	alert(`Your zodiac sign is : ${zodiacSigns.scorpio}`);
}
else if ((month === 11 && day >= 23) || (month === 12 && day <= 21)){
	alert(`Your zodiac sign is : ${zodiacSigns.sagittarius}`);
}
else ((month === 12 && day >= 22) || (month === 1 && day <= 20));
	alert(`Your zodiac sign is : ${zodiacSigns.capricorn}`);


let startYear = 1901; // відправна дата коли, наприклад, був рік Бика

x = (startYear - year) % 12;
switch (true) {
	case ( x == 1 || x == -11 ):
		alert("Considering the chinеse zodiac sign, you were born in a year of Rat - Щура");
		break;
	case (x == 0):
		alert("Considering the chinеse zodiac sign, you were born in a year of Ox - Бик");
		break;
	case (x == 11 || x == -1):
		alert("Considering the chinеse zodiac sign, you were born in a year of Tiger - Тигра");
		break;
	case (x == 10 || x == -2):
		alert("Considering the chinеse zodiac sign, you were born in a year of Rabbit - Кролика");
		break;
	case (x == 9 || x == -3):
		alert("Considering the chinеse zodiac sign, you were born in a year of Dragon - Дракона");
		break;
	case (x == 8 || x == -4):
		alert("Considering the chinеse zodiac sign, you were born in a year of Snake - Змії");
		break;
	case (x == 7 || x == -5):
		alert("Considering the chinеse zodiac sign, you were born in a year of Horse - Коня");
		break;
	case (x == 6 || x == -6):
		alert("Considering the chinеse zodiac sign, you were born in a year of Sheep - Вівці");
		break;
	case (x == 5 || x == -7):
		alert("Considering the chinеse zodiac sign, you were born in a year of Monkey - Мавпи");
		break;
	case (x == 4 || x == -8):
		alert("Considering the chinеse zodiac sign, you were born in a year of Rooster - Півня");
		break;
	case (x == 3 || x == -9):
		alert("Considering the chinеse zodiac sign, you were born in a year of Dog - Собаки");
		break;
	case (x == 2 || x == -10):
		alert("Considering the chinеse zodiac sign, you were born in a year of Pig - Свині");
		break;
	default:
		alert("You weren't borne on this Planet");
}